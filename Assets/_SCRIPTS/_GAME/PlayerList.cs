using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TNet;

public class PlayerList : MonoBehaviour {
	
	public static PlayerList instance;
	public System.Collections.Generic.List<GameObject> playerList;
	// Use this for initialization
	
	void Awake() 
	{
	if (TNManager.isHosting)
		instance = this;
	}
	
	void Start () {
		playerList = new System.Collections.Generic.List<GameObject>();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void AddPlayer()
	{
	//tno.Send (32, TNet.Target.All, value);
	}
	
	
	[RFC(32)]
	void OnAddPlayer (GameObject plr)
	{
		playerList.Add(plr);
	}
	
}
