using UnityEngine;
using TNet;

public class BawxScript : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
		GameObject spawnerObject = GameObject.Find("Spawner");
		if(spawnerObject.GetComponent("BawxSpawner"))
		{
			BawxSpawner spawnComponent = spawnerObject.GetComponent("BawxSpawner") as BawxSpawner;
			spawnComponent.bawxList.Add(this.gameObject);
		}
	}
}
