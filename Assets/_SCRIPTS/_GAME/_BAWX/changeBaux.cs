using UnityEngine;
using System.Collections;

public class changeBaux : MonoBehaviour {
	
	public static int BauxPoints = 0;
	public Texture2D bauxGUI;
	private GameObject baux;
	private Vector3 Pos;
	private float dist;
	
	private bool showGUI = false;
	
	private GameObject[] bawxs;
	private GameObject closest;
	
	private float distance;
	private Vector3 position;
	
	// Use this for initialization
	void Start () {
	
		Screen.showCursor = false;
		bawxs = GameObject.FindGameObjectsWithTag("Baux");
		closest = GameObject.FindWithTag("Baux");
		
	}
	
	// Update is called once per frame
	void Update () {
		
		FindClosestBaux();
		
		if(Input.GetKeyUp("e"))
		{
			baux = closest;
			if(baux)
			{
				Pos = baux.transform.position - this.transform.position;
				dist = Pos.sqrMagnitude;
			}
		}
		
		if(showGUI == false)
		{
			if(baux)
			{
				if(Input.GetKeyUp("e")&& dist < 10)
				{
					print("HELP ME TAWM CRUISE");
					showBoxBuild();
					print (showGUI);
				}
			}
		}
		else
		{
			if(Input.GetKeyUp("e")||Input.GetMouseButtonUp(0))
			{
				showBoxBuild();
				Destroy(baux);
				showGUI = false;
				print (showGUI);
			}
		}
	}
	
	void FindClosestBaux() {
		bawxs = GameObject.FindGameObjectsWithTag("Baux");
		closest = GameObject.FindWithTag("Baux");
		distance = Mathf.Infinity;
		position = transform.position;
		foreach(GameObject Baux in bawxs)
		{
			Vector3 diff = Baux.transform.position - position;
			float curDistance = diff.sqrMagnitude;
			if(curDistance < distance)
			{
				closest = Baux;
				distance = curDistance;
			}
		}
//		return closest;
	}
	
	void showBoxBuild()
	{
		if(showGUI == true)
			showGUI = false;
		else
			showGUI = true;
		//print (showGUI);
		//freeze camera
		if(showGUI == false)
			gameObject.GetComponent<ThirdPersonCamera>().enabled = true;
		else
			gameObject.GetComponent<ThirdPersonCamera>().enabled = false;
		//stop animation
		Animation anim = gameObject.GetComponent<Animation>();
		anim.CrossFade("idle");
		//show mouse
		if(showGUI == false)
			Screen.showCursor = false;
		else
			Screen.showCursor = true;
		//freeze player
		if(showGUI == false)
			gameObject.GetComponent<ThirdPersonController>().enabled = true;
		else
			gameObject.GetComponent<ThirdPersonController>().enabled = false;
		
	}
	
	void OnGUI ()
	{
		if(showGUI == true)
		{
			DrawGUI();
		}
	}
	
	void DrawGUI()
	{
		//GUI.Box(new Rect(10,10,200,100), "TEST");
		GUI.Box(new Rect(Screen.width/2 - bauxGUI.width/2, Screen.height/2 - bauxGUI.height/2, bauxGUI.width, bauxGUI.height),bauxGUI);
		GUI.Button(new Rect(Screen.width/2 - bauxGUI.width/2 + 75, Screen.height/2 - bauxGUI.height/2 + 20, 250, 100),"WEAPON 1");
		GUI.Button(new Rect(Screen.width/2 - bauxGUI.width/2 + 685, Screen.height/2 - bauxGUI.height/2 + 20, 250, 100),"WEAPON 2");
		GUI.Button(new Rect(Screen.width/2 - bauxGUI.width/2 + 75, Screen.height/2 - bauxGUI.height/2 + 145, 250, 100),"WEAPON 3");
		GUI.Button(new Rect(Screen.width/2 - bauxGUI.width/2 + 685, Screen.height/2 - bauxGUI.height/2 + 145, 250, 100),"WEAPON 4");
		GUI.Button(new Rect(Screen.width/2 - bauxGUI.width/2 + 75, Screen.height/2 - bauxGUI.height/2 + 270, 250, 100),"WEAPON 5");
		GUI.Button(new Rect(Screen.width/2 - bauxGUI.width/2 + 685, Screen.height/2 - bauxGUI.height/2 + 270, 250, 100),"WEAPON 6");
		GUI.Button(new Rect(Screen.width/2 - bauxGUI.width/2 + 75, Screen.height/2 - bauxGUI.height/2 + 395, 250, 100),"WEAPON 7");
		GUI.Button(new Rect(Screen.width/2 - bauxGUI.width/2 + 685, Screen.height/2 - bauxGUI.height/2 + 395, 250, 100),"WEAPON 8");
	}
}
