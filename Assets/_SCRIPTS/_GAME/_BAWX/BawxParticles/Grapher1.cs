using UnityEngine;
using System.Collections;

public class Grapher1 : MonoBehaviour {
	
	public enum FunctionOption
	{
		Linear,
		Exponential,
		Parabola,
		Sine
	}
	
	private delegate float FunctionDelegate(float x);
	
	private static FunctionDelegate[] functionDelegates = 
	{
		Linear,
		Exponential,
		Parabola,
		Sine
	};
	
	
	public FunctionOption function;
	public int resolution = 10;
	
	private int currResolution;
	
	private ParticleSystem.Particle[] points;
	
	// Use this for initialization
	void Start () {
		
		CreatePoints();
		
	}
	
	// Update is called once per frame
	void Update () {
			
		if(currResolution != resolution)
			CreatePoints();
		
		FunctionDelegate f = functionDelegates[(int)function];
		for(int i = 0; i < resolution; i++)
		{
			Vector3 pos = points[i].position;
			pos.y = f(pos.x);
			points[i].position = pos;
			Color green = points[i].color;
			green.g = pos.y;
			points[i].color = green;
		}
		
		particleSystem.SetParticles(points, points.Length);
		
	}
	
	private static float Linear(float x)
	{
		return x;
	}
	
	private static float Exponential(float x)
	{
		return x*x;	
	}
	
	private static float Parabola(float x)
	{
		x = 2f * x - 1f;
		return x*x;
	}
	
	private static float Sine(float x)
	{
		return 0.5f + (0.5f*Mathf.Sin(2*Mathf.PI*(x+Time.timeSinceLevelLoad)));
	}
	
	private void CreatePoints()
	{
		if(resolution > 1000)
			resolution = 1000;
		else if(resolution < 2)
			resolution = 2;
		
		currResolution = resolution;
		
		points = new ParticleSystem.Particle[resolution];
		float increment = 1f / (resolution - 1);
		for(int i = 0; i < resolution; i++)
		{
			float x = i * increment;
			points[i].position = new Vector3(x, 0f, 0f);
			points[i].color = new Color(x, 0f, 0f);
			points[i].size = 0.1f;
		}
	}
}
