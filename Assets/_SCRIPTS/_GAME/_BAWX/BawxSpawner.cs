using UnityEngine;
using TNet;

public class BawxSpawner : TNBehaviour{
	
	private float timeSpawn, randomX, randomY;
	public List<GameObject> bawxList;
	private GameObject[] bawxArray;
	
	public bool UNSTABLE_MODE;
	
	public int bawxSpawn, maxNumBawx;
	public float spawnTime;
	public GameObject bawx, spawner;
	
	public static int maxBawxes;
	public static int numBawxes = 0;
	
	void Start()
	{
		timeSpawn = spawnTime;
		maxBawxes = maxNumBawx;
		bawxArray = GameObject.FindGameObjectsWithTag("Bawx");
		if(bawxArray.Length > 0)
		{
			bawxList = new List<GameObject>();
			foreach(GameObject tBawx in bawxArray)
			{
				bawxList.Add(tBawx);
			}
		}
		else
		{
			bawxList = new List<GameObject>();
		}
		
		//UNSTABLE_MODE = false;
	}
	
	void Update()
	{
		if(TNManager.isHosting)
		{
			if(UNSTABLE_MODE)
			{
				randomX = Random.Range(spawner.transform.position.x-(spawner.transform.localScale.x/2f), spawner.transform.position.x+(spawner.transform.localScale.x/2f));
				randomY = Random.Range(spawner.transform.position.z-(spawner.transform.localScale.z/2f), spawner.transform.position.z+(spawner.transform.localScale.z/2f));
				//Vector3 position1 = new Vector3(-randomX,spawner.transform.position.y,randomY);
				Vector3 position2 = new Vector3(randomX,spawner.transform.position.y,randomY);
				//Instantiate(bawx, position1, Quaternion.identity);
				Instantiate(bawx, position2, Quaternion.identity);
			}
			else
			{
				if(numBawxes < maxBawxes)
				{
					if(spawnTime > 0)
					{
						spawnTime -= Time.deltaTime;
					}
					else
					{
						for(int i = 0; i < bawxSpawn; i++)
						{
							randomX = Random.Range(spawner.transform.position.x-(spawner.transform.localScale.x/2f), spawner.transform.position.x+(spawner.transform.localScale.x/2f));
							randomY = Random.Range(spawner.transform.position.z-(spawner.transform.localScale.z/2f), spawner.transform.position.z+(spawner.transform.localScale.z/2f));
							//Vector3 position1 = new Vector3(-randomX,spawner.transform.position.y,randomY);
							Vector3 position2 = new Vector3(randomX,spawner.transform.position.y,randomY);
							//Instantiate(bawx, position1, Quaternion.identity);
							TNManager.Create(bawx,position2,Quaternion.Euler(0,0,0),false);
							numBawxes++;
						}
						spawnTime = timeSpawn;
					}
					//print (bawxList.size);
				}
			}
		}
	}

}