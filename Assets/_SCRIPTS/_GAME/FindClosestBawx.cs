using UnityEngine;
using System.Collections;

public class FindClosestBawx : MonoBehaviour{
	private GameObject[] bawxs;
	
	private float distance;
	private Vector3 position;
	
	void Start()
	{
		bawxs = GameObject.FindGameObjectsWithTag("Baux");
	}
	
	void Update()
	{
		FindClosestBaux();
	//	print(FindClosestBaux().closest);
	}
	
	void FindClosestBaux() {
		bawxs = GameObject.FindGameObjectsWithTag("Baux");
		GameObject closest = GameObject.FindWithTag("Baux");
		distance = Mathf.Infinity;
		position = transform.position;
		foreach(GameObject Baux in bawxs)
		{
			Vector3 diff = Baux.transform.position - position;
			float curDistance = diff.sqrMagnitude;
			if(curDistance < distance)
			{
				closest = Baux;
				distance = curDistance;
			}
		}
	}
}

/*
public class example : MonoBehaviour {
    GameObject FindClosestBawx() {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("Enemy");
        GameObject closest;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gos) {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance) {
                closest = go;
                distance = curDistance;
            }
			return closest;
        }
    }
    void Example() {
        print(FindClosestBaux().name);
    }
}
*/