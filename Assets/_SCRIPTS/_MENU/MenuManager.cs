using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Threading;

public class MenuManager : MonoBehaviour 
{
	static MenuManager _instance = null;
	public static MenuManager instance
	{
		get
		{
			if(!_instance)
			{
				_instance = FindObjectOfType(typeof(MenuManager))as MenuManager;
				
				if(!_instance)
				{
					var obj = new GameObject ("_MenuManager");
					_instance = obj.AddComponent<MenuManager>();
				}
			}
			return _instance;
		}
	}
	private GameObject tnManagerObject;
	private HostJoinGUI hostJoinGUI;
	
	private GameObject mainCamera;
	private Vector3 originalCameraPosition;
	private Vector3 originalCameraRotation;
	private GameObject startMenuCameraDestination;
	
	private GameObject mainButton;
	private Vector3 originalMainButtonPosition;
	private GameObject leftButton;
	private Vector3 originalLeftButtonPosition;
	private GameObject rightButton;
	private Vector3 originalRightButtonPosition;
	
	private GameObject bawxPivot;
	
	private GameObject startMesh;
	private GameObject optionsMesh;
	private GameObject customizeMesh;
	private GameObject creditsMesh;
	
	private int mainMenuState = 0;
	private bool inSubMenu = false;
	private bool canClick = false;
	
	public float bawxSpinDuration = 0.8f;
	public float cameraMoveDuration = 1.2f;
	public float cameraRotationDuration = 1.6f;
	
	void Awake() 
	{
		//Listen to Events
		Clicker.Instance.OnClick += OnClick;
		
		//Find Objects
		tnManagerObject = GameObject.Find("/TNManager");
		hostJoinGUI = tnManagerObject.GetComponent<HostJoinGUI>();
		
		mainCamera = GameObject.Find("_MainCamera");
		originalCameraPosition = mainCamera.transform.localPosition;
		originalCameraRotation = mainCamera.transform.localEulerAngles;
		
		startMenuCameraDestination = GameObject.Find("/MenuCameraPositions/StartMenuCameraDestination");
		
		bawxPivot = GameObject.Find("BawxPivot");
		
		mainButton = GameObject.Find("/_MainCamera/ButtonMeshOrigin");
		originalMainButtonPosition = mainButton.transform.localPosition;
		leftButton = GameObject.Find("/_MainCamera/LeftButton");
		originalLeftButtonPosition = leftButton.transform.localPosition;
		rightButton = GameObject.Find("/_MainCamera/RightButton");
		originalRightButtonPosition = rightButton.transform.localPosition;
		
		startMesh = GameObject.Find("/_MainCamera/ButtonMeshOrigin/StartMesh");
		optionsMesh = GameObject.Find("/_MainCamera/ButtonMeshOrigin/OptionsMesh");
		customizeMesh = GameObject.Find("/_MainCamera/ButtonMeshOrigin/CustomizeMesh");
		creditsMesh = GameObject.Find("/_MainCamera/ButtonMeshOrigin/CreditsMesh");
		
		hostJoinGUI.enabled = false;
		optionsMesh.SetActive(false);
		customizeMesh.SetActive(false);
		creditsMesh.SetActive(false);
		
		//Initializer
		canClick = true;
	}
	
	void OnClick(GameObject g)
	{
		switch(g.name)
		{
		case "LeftArrow":
			if(canClick)
			{
				if(!inSubMenu)
				{
					switch(mainMenuState)
					{
					case 0:
						var moveMain0 = MainMenuJob.make(moveTo(mainButton, new Vector3(0f,-4.0f,0f), bawxSpinDuration/2, Easing.Quartic.easeIn));
						moveMain0.jobComplete += (wasKilled) =>
						{
							startMesh.SetActive(false);
							optionsMesh.SetActive(false);
							customizeMesh.SetActive(false);
							creditsMesh.SetActive(true);
						};
						var moveMainBack0 = MainMenuJob.make(moveTo(mainButton, originalMainButtonPosition, bawxSpinDuration/2, Easing.Quartic.easeIn));
						break;
					case 1:
						var moveMain1 = MainMenuJob.make(moveTo(mainButton, new Vector3(0f,-4.0f,0f), bawxSpinDuration/2, Easing.Quartic.easeIn));
						moveMain1.jobComplete += (wasKilled) =>
						{
							startMesh.SetActive(true);
							optionsMesh.SetActive(false);
							customizeMesh.SetActive(false);
							creditsMesh.SetActive(false);
						};
						var moveMainBack1 = MainMenuJob.make(moveTo(mainButton, originalMainButtonPosition, bawxSpinDuration/2, Easing.Quartic.easeIn));
						break;
					case 2:
						var moveMain2 = MainMenuJob.make(moveTo(mainButton, new Vector3(0f,-4.0f,0f), bawxSpinDuration/2, Easing.Quartic.easeIn));
						moveMain2.jobComplete += (wasKilled) =>
						{
							startMesh.SetActive(false);
							optionsMesh.SetActive(true);
							customizeMesh.SetActive(false);
							creditsMesh.SetActive(false);
						};
						var moveMainBack2 = MainMenuJob.make(moveTo(mainButton, originalMainButtonPosition, bawxSpinDuration/2, Easing.Quartic.easeIn));
						break;
					case 3:
						var moveMain3 = MainMenuJob.make(moveTo(mainButton, new Vector3(0f,-4.0f,0f), bawxSpinDuration/2, Easing.Quartic.easeIn));
						moveMain3.jobComplete += (wasKilled) =>
						{
							startMesh.SetActive(false);
							optionsMesh.SetActive(false);
							customizeMesh.SetActive(true);
							creditsMesh.SetActive(false);
						};
						var moveMainBack3 = MainMenuJob.make(moveTo(mainButton, originalMainButtonPosition, bawxSpinDuration/2, Easing.Quartic.easeIn));
						break;
					}
					
					var moveLeft = MainMenuJob.make(rotateAdd(bawxPivot, true, 90.0f, bawxSpinDuration));
					moveLeft.jobComplete += (wasKilled) =>
					{
						FinishMovingLeft();	
					};
				}
				else
				{
					hostJoinGUI.enabled = false;
					var moveMain = MainMenuJob.make(moveTo(mainButton, originalMainButtonPosition, cameraMoveDuration, Easing.Quartic.easeIn));
					var moveCamera = MainMenuJob.make(moveTo(mainCamera, originalCameraPosition, cameraMoveDuration, Easing.Quartic.easeIn));
					var rotateCamera = MainMenuJob.make(rotateTo(mainCamera, true, originalCameraRotation, cameraRotationDuration, Easing.Quartic.easeIn));
					var moveLeftButton = MainMenuJob.make(moveTo(leftButton, originalLeftButtonPosition, cameraMoveDuration, Easing.Quartic.easeIn));
					var moveRightButton = MainMenuJob.make(moveTo(rightButton, originalRightButtonPosition, cameraMoveDuration, Easing.Quartic.easeIn));
					inSubMenu = false;
				}
			}
			break;
		case "RightArrow":
			if(canClick)
			{
				if(!inSubMenu)
				{
					switch(mainMenuState)
					{
					case 0:
						var moveMain0 = MainMenuJob.make(moveTo(mainButton, new Vector3(0f,-4.0f,0f), bawxSpinDuration/2, Easing.Quartic.easeIn));
						moveMain0.jobComplete += (wasKilled) =>
						{
							startMesh.SetActive(false);
							optionsMesh.SetActive(true);
							customizeMesh.SetActive(false);
							creditsMesh.SetActive(false);
						};
						var moveMainBack0 = MainMenuJob.make(moveTo(mainButton, originalMainButtonPosition, bawxSpinDuration/2, Easing.Quartic.easeIn));
						break;
					case 1:
						var moveMain1 = MainMenuJob.make(moveTo(mainButton, new Vector3(0f,-4.0f,0f), bawxSpinDuration/2, Easing.Quartic.easeIn));
						moveMain1.jobComplete += (wasKilled) =>
						{
							startMesh.SetActive(false);
							optionsMesh.SetActive(false);
							customizeMesh.SetActive(true);
							creditsMesh.SetActive(false);
						};
						var moveMainBack1 = MainMenuJob.make(moveTo(mainButton, originalMainButtonPosition, bawxSpinDuration/2, Easing.Quartic.easeIn));
						break;
					case 2:
						var moveMain2 = MainMenuJob.make(moveTo(mainButton, new Vector3(0f,-4.0f,0f), bawxSpinDuration/2, Easing.Quartic.easeIn));
						moveMain2.jobComplete += (wasKilled) =>
						{
							startMesh.SetActive(false);
							optionsMesh.SetActive(false);
							customizeMesh.SetActive(false);
							creditsMesh.SetActive(true);
						};
						var moveMainBack2 = MainMenuJob.make(moveTo(mainButton, originalMainButtonPosition, bawxSpinDuration/2, Easing.Quartic.easeIn));
						break;
					case 3:
						var moveMain3 = MainMenuJob.make(moveTo(mainButton, new Vector3(0f,-4.0f,0f), bawxSpinDuration/2, Easing.Quartic.easeIn));
						moveMain3.jobComplete += (wasKilled) =>
						{
							startMesh.SetActive(true);
							optionsMesh.SetActive(false);
							customizeMesh.SetActive(false);
							creditsMesh.SetActive(false);
						};
						var moveMainBack3 = MainMenuJob.make(moveTo(mainButton, originalMainButtonPosition, bawxSpinDuration/2, Easing.Quartic.easeIn));
						break;
					}
					
					var moveRight = MainMenuJob.make(rotateAdd(bawxPivot, false, 90.0f, bawxSpinDuration));
					moveRight.jobComplete += (wasKilled) =>
					{
						FinishMovingRight();
					};
				}
				else
				{
					
				}
			}
			break;
		case "StartButton":
			if(canClick)
			{
				if(mainMenuState == 0 && !inSubMenu)
				{
					var moveMain = MainMenuJob.make(moveTo(mainButton, new Vector3(0f,-4.0f,0f), cameraMoveDuration, Easing.Quartic.easeIn));
					var moveCamera = MainMenuJob.make(moveTo(mainCamera, startMenuCameraDestination.transform.localPosition, cameraMoveDuration, Easing.Quartic.easeIn));
					moveCamera.jobComplete += (wasKilled) =>
					{
						hostJoinGUI.enabled = true;	
					};
					var rotateCamera = MainMenuJob.make(rotateTo(mainCamera, false, startMenuCameraDestination.transform.localEulerAngles, cameraRotationDuration, Easing.Quartic.easeIn));
					var moveLeftButton = MainMenuJob.make(moveTo(leftButton, (leftButton.transform.localPosition + new Vector3(0f,0.6f,0f)), cameraMoveDuration, Easing.Quartic.easeIn));
					var moveRightButton = MainMenuJob.make(moveTo(rightButton, (rightButton.transform.localPosition + new Vector3(0f,-1.2f,0f)), cameraMoveDuration, Easing.Quartic.easeIn));
					inSubMenu = true;
				}
			}
			break;
		case "OptionsButton":
			if(canClick)
			{
				if(mainMenuState == 1)
				{
					
				}
			}
			break;
		case "CustomizeButton":
			if(canClick)
			{
				if(mainMenuState == 2)
				{
					
				}
			}
			break;
		case "CreditsButton":
			if(canClick)
			{
				if(mainMenuState == 3)
				{
					
				}
			}
			break;
		}
	}
	
	private IEnumerator rotateAdd(GameObject go, bool clockwise, float rotation, float duration)
	{
		canClick = false;
		
		int leftOrRight = 0;
		
		float amountRemaining = rotation;
		float amountToRotate = 0.0f;
		
		var startTime = Time.time;
		var endTime = startTime + duration;
		var currentTime = Time.time;
		
		float easePosition;
		
		if(clockwise)
			leftOrRight = -1;
		else
			leftOrRight = 1;
		
		// loop until we reach endTime
		while( currentTime <= endTime && amountRemaining >= 0 )
		{
			// calculate our easePosition which will always be between 0 and 1
			easePosition = Mathf.Clamp01( ( Time.time - startTime ) / duration );
			
			amountToRotate = amountRemaining * easePosition;
			go.transform.localEulerAngles = new Vector3(go.transform.rotation.eulerAngles.x, go.transform.rotation.eulerAngles.y + amountToRotate * leftOrRight, go.transform.rotation.eulerAngles.z);
			amountRemaining -= amountToRotate;
			
			currentTime = Time.time;
			yield return null;
		}
	}
	private IEnumerator rotateTo(GameObject go, bool clockwise, Vector3 rotation, float duration)
	{
		return rotateTo(go, clockwise, rotation, duration, Easing.Linear.easeIn);
	}
	private IEnumerator rotateTo(GameObject go, bool clockwise, Vector3 rotation, float duration, System.Func<float, float> ease)
	{
		int leftOrRight = 0;
		
		var startTime = Time.time;
		var endTime = startTime + duration;
		var currentTime = Time.time;
		
		float easePosition;
		
		if(clockwise)
			leftOrRight = -1;
		else
			leftOrRight = 1;
		
		while( currentTime <= endTime )
		{
			easePosition = Mathf.Clamp01( ( Time.time - startTime ) / duration );
			
			go.transform.localEulerAngles = Vector3.Lerp(go.transform.localEulerAngles, rotation, ease(easePosition));
			
			currentTime = Time.time;
			yield return null;
		}
	}
	private IEnumerator moveTo(GameObject go, Vector3 destination, float duration)
	{
		return moveTo(go, destination, duration, Easing.Linear.easeIn);
	}
	private IEnumerator moveTo(GameObject go, Vector3 destination, float duration, System.Func<float, float> ease)
	{
		var startTime = Time.time;
		var endTime = startTime + duration;
		var currentTime = Time.time;
		
		float easePosition;
		
		while( currentTime <= endTime )
		{
			easePosition = Mathf.Clamp01( ( Time.time - startTime ) / duration );
			
			go.transform.localPosition = Vector3.Lerp(go.transform.localPosition, destination, ease(easePosition));
			
			currentTime = Time.time;
			
			yield return null;
		}
	}
	void Update() {
	if (Input.GetKey(KeyCode.T))
		Application.LoadLevel("testPlayerStuff");
	}
	
	void FinishMovingRight()
	{
		if(mainMenuState == 3)
		{
			mainMenuState = 0;
		}
		else
			mainMenuState++;
		
		print (mainMenuState);
		canClick = true;
	}
	void FinishMovingLeft()
	{
		if(mainMenuState == 0)
		{
			mainMenuState = 3;
		}
		else
			mainMenuState--;
		
		print (mainMenuState);
		canClick = true;
	}
	
	void OnApplicationQuit()
	{
		_instance = null;	
	}
}