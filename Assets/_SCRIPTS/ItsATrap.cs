using UnityEngine;
using System.Collections;

public class ItsATrap : MonoBehaviour {

//. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . _________
//. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . ./ It’s a trap! \
//. . . . . . . . . . . . . . . . _,,,--~~~~~~~~--,_ . . . .\ ._________/
//. . . . . . . . . . . . . . ,-‘ : : : :::: :::: :: : : : : :º ‘-, . . \/. . . . . . . . . .
//. . . . . . . . . . . . .,-‘ :: : : :::: :::: :::: :::: : : :o : ‘-, . . . . . . . . . .
//. . . . . . . . . . . ,-‘ :: ::: :: : : :: :::: :::: :: : : : : :O ‘-, . . . . . . . . .
//. . . . . . . . . .,-‘ : :: :: :: :: :: : : : : : , : : :º :::: :::: ::’; . . . . . . . .
//. . . . . . . . .,-‘ / / : :: :: :: :: : : :::: :::-, ;; ;; ;; ;; ;; ;; ;\ . . . . . . . .
//. . . . . . . . /,-‘,’ :: : : : : : : : : :: :: :: : ‘-, ;; ;; ;; ;; ;; ;;| . . . . . . .
//. . . . . . . /,’,-‘ :: :: :: :: :: :: :: : ::_,-~~,_’-, ;; ;; ;; ;; | . . . . . . .
//. . . . . _/ :,’ :/ :: :: :: : : :: :: _,-‘/ : ,-‘;’-‘’’’’~-, ;; ;; ;;,’ . . . . . . . .
//. . . ,-‘ / : : : : : : ,-‘’’ : : :,--‘’ :|| /,-‘-‘--‘’’__,’’’ \ ;; ;,-‘ . . . . . . . .
//. . . \ :/,, : : : _,-‘ --,,_ : : \ :\ ||/ /,-‘-‘x### ::\ \ ;;/ . . . . . . . . . .
//. . . . \/ /---‘’’’ : \ #\ : :\ : : \ :\ \| | : (O##º : :/ /-‘’ . . . . . . . . . . .
//. . . . /,’____ : :\ ‘-#\ : \, : :\ :\ \ \ : ‘-,___,-‘,-`-,, . . . . . . . . . . .
//. . . . ‘ ) : : : :’’’’--,,--,,,,,,¯ \ \ :: ::--,,_’’-,,’’’¯ :’- :’-, . . . . . . . . .
//. . . . .) : : : : : : ,, : ‘’’’~~~~’ \ :: :: :: :’’’’’¯ :: ,-‘ :,/\ . . . . . . . . .
//. . . . .\,/ /|\\| | :/ / : : : : : : : ,’-, :: :: :: :: ::,--‘’ :,-‘ \ \ . . . . . . . .
//. . . . .\\’|\\ \|/ ‘/ / :: :_--,, : , | )’; :: :: :: :,-‘’ : ,-‘ : : :\ \, . . . . . . .
//. . . ./¯ :| \ |\ : |/\ :: ::----, :\/ :|/ :: :: ,-‘’ : :,-‘ : : : : : : ‘’-,,_ . . . .
//. . ..| : : :/ ‘’-(, :: :: :: ‘’’’’~,,,,,’’ :: ,-‘’ : :,-‘ : : : : : : : : :,-‘’’\\ . . . .
//. ,-‘ : : : | : : ‘’) : : :¯’’’’~-,: : ,--‘’’ : :,-‘’ : : : : : : : : : ,-‘ :¯’’’’’-,_ .
//./ : : : : :’-, :: | :: :: :: _,,-‘’’’¯ : ,--‘’ : : : : : : : : : : : / : : : : : : :’’-,
/// : : : : : -, :¯’’’’’’’’’’’¯ : : _,,-~’’ : : : : : : : : : : : : : :| : : : : : : : : :
// : : : : : : :¯’’~~~~~~’’’ : : : : : : : : : : : : : : : : : : | : : : : : : : : : 
}