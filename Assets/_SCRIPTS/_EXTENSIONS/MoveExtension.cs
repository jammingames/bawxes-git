using UnityEngine;
using System.Collections;

public static class MoveExtensions {

#region TransformPosition

public static void moveX ( this Transform tran, float offsetX )
{
	Vector3 temp = tran.position;
	temp.x += offsetX;
	tran.position = temp;
}

public static void moveY ( this Transform tran, float offsetY )
{
	Vector3 temp = tran.position;
	temp.y += offsetY;
	tran.position = temp;
}

public static void moveZ ( this Transform tran, float offsetZ )
{
	Vector3 temp = tran.position;
	temp.z += offsetZ;
	tran.position = temp;
}

#endregion TransformPosition

}