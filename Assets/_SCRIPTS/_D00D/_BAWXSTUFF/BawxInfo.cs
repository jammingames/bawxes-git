using UnityEngine;
using System.Collections;
using TNet;

public class BawxInfo : TNBehaviour {
	
	
	private bool mCanGrab = true;
	public bool canGrab
	{
		set{tno.Send(003, TNet.Target.AllSaved, value);}
		get{return mCanGrab;}
	}
	
	
		[RFC(003)]
	void OnGrab(bool val)
	{
		mCanGrab = val;
		
	}
	
}
