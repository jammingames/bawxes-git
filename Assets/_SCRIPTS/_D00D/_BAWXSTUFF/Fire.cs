using UnityEngine;
using System.Collections;
using TNet;

public class Fire : ThrowBawxes 
{
	
	

	public float smooth = 3f;		
	public AudioClip sound;
	public AudioClip sound2;
	
	
	private float timer = 6.0f;
	private float startTime;
	private Transform p;
	public Transform standardPos;
	
	
	private bool mIsThrowingBawx = false;
	public bool isThrowingBawx
	{
		set{tno.Send(012, TNet.Target.AllSaved, value);}
		get {return mIsThrowingBawx;}
	}
	
	private bool mIsGrabbingBawx = false;
	public bool isGrabbingBawx
	{
		set{tno.Send(011, TNet.Target.AllSaved, value);}
		get {return mIsGrabbingBawx;}
	}
	
	private bool mIsHoldingBawx = false;
	public bool isHoldingBawx 
	{
		set{tno.Send(010, TNet.Target.AllSaved, value);}
		get {return mIsHoldingBawx;}
	}


	
	
	
		
	void Start()
	{
		startTime = timer;
		if (tno.isMine)
		{
			InputManager.Instance.OnInput += OnInput;
		}
	}
	
	
	void FixedUpdate()
	{
		HoldBawx();
	
	}
		
		
	void Update()
	{
		if (mIsGrabbingBawx && timer >= 0)
		{
			timer--;
			//start timer for bawx is trying to be grabbed
		}
		else if (mIsGrabbingBawx && timer <= 0)
		{
			isGrabbingBawx = false;
			timer = startTime;
		}
		
		if (mIsThrowingBawx && CheckFire())
		{
			StartCoroutine("PerformFire");
			timer = startTime;
		}
		
	}
	
	void OnTriggerStay(Collider other)
	{
		if (other.tag == "Bawx" && mIsGrabbingBawx)
		{
			GrabBawx(other.transform);
		}
			//do stuff
	
	}
	
	bool CheckFire()
	{
		if (!mIsHoldingBawx) return false;
		else if (mIsHoldingBawx) return true;
		else return false;
	}
	
	IEnumerator PerformFire()
	{
		p.parent = null;
		Fire();
		ControlManager.instance.currStam -= 20f;
		p.GetComponent<BawxInfo>().canGrab = true;
		yield return new WaitForSeconds(1);
		isThrowingBawx = false;
		isHoldingBawx = false;
	}
	
	void GrabBawx(Transform bawx)
	{
			p = bawx;
			projectile = p;
			isGrabbingBawx = false;
			isHoldingBawx = true;
			timer = startTime;
			p.GetComponent<BawxInfo>().canGrab = false;
	}
	
	void HoldBawx()
	{
			
		if (mIsHoldingBawx == true && mIsThrowingBawx == false) 
		{
			if (p && standardPos)
			{
				p.position = standardPos.position;
				p.parent = standardPos.transform;
				p.rigidbody.useGravity = false;
		//		p.rigidbody.AddTorque(Vector3.back * 10);
			}	
		}
	}
	
	void OnInput(string input, bool down) 
	{
		if(down && input == "E")
		{
			if (mIsHoldingBawx)
			{
				if(!mIsThrowingBawx)
				isThrowingBawx = true;
			}
			if(!mIsGrabbingBawx)
			{
				isGrabbingBawx = true;
			}
			
		}
		//else if ()
	}
	
	
	
	[RFC(010)]
	void OnSetHoldingBawx(bool val)
	{
		mIsHoldingBawx = val;
	}
	
	[RFC(011)]
	void OnSetGrabbingBawx(bool val)
	{
		mIsGrabbingBawx = val;
	}
	
		
	[RFC(012)]
	void OnSetThrowingBawx(bool val)
	{
		mIsThrowingBawx = val;
	}
	
}