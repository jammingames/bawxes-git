using UnityEngine;

using System.Collections;

public class ThrowBawxes : Bawxes
{
	
	public float firingAngle = 45.0f;
	public float gravity = 9.8f;
	public float force = 200.0f;
	private Transform Target;
	private Transform Projectile;
	private Transform myTransform;
	
	public Transform target {
		get { return Target; }
		set { Target = value; }
	}
	
	public Transform projectile {
		get { return Projectile; }
		set { Projectile = value; }
	}
	
	void Awake ()
	{
//		Target = GameObject.Find("LookAtPos").transform;	
		myTransform = this.transform;
	}

	public void Fire ()
	{
		StartCoroutine (SimulateProjectile());
		SimulateProjectile();
	}
	 
public IEnumerator SimulateProjectile ()

	{
		projectile.rigidbody.velocity = Vector3.zero;
		Vector3 dir = new Vector3(0,force,force*1.5f);
		projectile.rotation = this.transform.rotation;
		projectile.rigidbody.AddRelativeForce(dir, ForceMode.Impulse);
		projectile.rigidbody.AddTorque(Vector3.forward * 30);
		projectile.rigidbody.useGravity = true;
		
		//Projectile.GetComponent<Rigidbody>().isKinematic = true;
		yield return new WaitForSeconds(0.4f);
		
	}
}
		/*
		float target_Distance = Vector3.Distance (Projectile.position, Target.position);
		float projectile_Velocity = target_Distance * (Mathf.Sin (2 * firingAngle * Mathf.Deg2Rad) * gravity);
		float Vx = Mathf.Sqrt (projectile_Velocity) * Mathf.Cos (firingAngle * Mathf.Deg2Rad);
		float Vy = Mathf.Sqrt (projectile_Velocity) * Mathf.Sin (firingAngle * Mathf.Deg2Rad);
		float flightDuration = target_Distance * Vx;

		Projectile.rotation = Quaternion.LookRotation (Target.position - Projectile.position);

		float elapse_time = 0;
		
		while (elapse_time < flightDuration) {

			Projectile.Translate (0, (Vy - (gravity * elapse_time)) * Time.deltaTime, Vx * Time.deltaTime);

           

			elapse_time += Time.deltaTime;
			yield return false;
		}
		
		if ((flightDuration - elapse_time) < flightDuration*10) Projectile.GetComponent<Rigidbody>().isKinematic = false;
		
		return false;
	}   
*/
