using UnityEngine;
using System.Collections;
using TNet;

public class SpawnStats : TNBehaviour {

	private int mID;
	public int iD
	{
		set{tno.Send(40, TNet.Target.Host, value);}
		get{return mID;}
	}
	
	void Start() 
	{
		if (tno.isMine)
		{
			iD = TNManager.playerID;
		}
	}
	
	
	
	[RFC(40)]
	void OnSetID(int v)
	{
		mID = v;
		print(v);
	}
}
