using UnityEngine;
using TNet;

public class ControlManager : TNBehaviour 
{
	static ControlManager _instance = null;
	public static ControlManager instance
	{
		get
		{
			if(!_instance)
			{
				_instance = FindObjectOfType(typeof(ControlManager))as ControlManager;
				
				if(!_instance)
				{
					var obj = new GameObject ("_ControlManager");
					_instance = obj.AddComponent<ControlManager>();
				}
			}
			return _instance;
		}
	}
	
	public float distance = 1.0f;
	public float speed = 10.0f;
	public float gravity = 10.0f;
	public float maxVelocityChange = 1.0f;
	
	public float jumpHeight = 2.0f;
	public float maxStam = 100f;
	public float currStam = 100f;
	public Animator animObj;
	
	private bool grounded = false;	
	public bool canJump = true;
	private bool moving = false;
	private bool sprintCD = false;
	
	
	private Vector3 mTargetVelocity = Vector3.zero;
	public Vector3 targetVelocity{set{tno.SendQuickly(4, TNet.Target.All, value);}}
	
	private float mAnimValue;
	public float animValue{set{tno.Send(013, TNet.Target.All, value);}}
	
	bool mJumping = false;
	public bool jumping{set{tno.Send (5, TNet.Target.All, value);}}
	
	private int mIsSprinting = 0;
	public int isSprinting{set{tno.Send(6, TNet.Target.All, value);}}
	
	
	void Start() 
	{
		rigidbody.freezeRotation = true;
		rigidbody.useGravity = false;
		
		if(tno.isMine)
		{
			InputManager.Instance.OnInput += OnInput;
			InputManager.Instance.OnInputAxeez += OnInputAxis;
		}
	}
	
	void Update () 
	{	
		if(sprintCD)
		{
			currStam++;
			if(currStam >= 100)
				sprintCD = false;
		}
		if(mIsSprinting == 1)
		{
			if(currStam > 0 && !sprintCD)
			{
				speed = 20f;
				currStam--;
			}
			if(currStam <= 0)
			{
				sprintCD = true;
				speed = 10f;
			}
		}
		else
		{
			if(currStam < maxStam)
			{
				speed = 10f;
				currStam++;
			}
		}
	}
	
	void FixedUpdate () 
	{
			
			if (Physics.Raycast(transform.position, Vector3.down, distance))
			{
				canJump = true;
				//print("I am groudned");
			}
			else 
			{
				canJump = false;
				//print("I am flying");
			}
		
		if (grounded) 
		{
			// Calculate how fast we should be moving
			Vector3 tVel = new Vector3(0, 0, 0);
			tVel = mTargetVelocity;
			tVel = transform.TransformDirection(mTargetVelocity);
			tVel *= speed;
			
			// Apply a force that attempts to reach our target velocity
			Vector3 velocity = rigidbody.velocity;
			Vector3 velocityChange = (tVel - velocity);
			if (moving)
			{
				velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
				velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
				velocityChange.y = 0;
				rigidbody.AddForce(velocityChange, ForceMode.VelocityChange);
			}
			else {
				//velocityChange.x = Vector3.
				velocityChange.x = Mathf.Clamp(velocityChange.x, 0, maxVelocityChange);
				velocityChange.z = Mathf.Clamp(velocityChange.z, 0, maxVelocityChange);
				velocityChange.y = rigidbody.velocity.y;
				rigidbody.velocity = velocityChange;
				//rigidbody.AddForce(velocityChange, ForceMode.VelocityChange)
			}
			// Jump
			if (mJumping && canJump) 
			{
				rigidbody.velocity = new Vector3(rigidbody.velocity.x, CalculateJumpVerticalSpeed(), rigidbody.velocity.z);
				
				mJumping = false;
				canJump = false;
				grounded = false;
			}
			
			
		}

		
		// We apply gravity manually for more tuning control
		rigidbody.AddForce(new Vector3 (0, -gravity * rigidbody.mass, 0));
		
		//grounded = false;
	}
	
	float CalculateJumpVerticalSpeed() 
	{
		// From the jump height and gravity we deduce the upwards speed 
		// for the character to reach at the apex.
		return Mathf.Sqrt(2 * jumpHeight * gravity);
	}
	
	void OnCollisionStay () 
	{
		//canJump = true;
		grounded = true; 
	}
	
	void OnNetworkPlayerJoin (Player p)
	{
		tno.Send(7, p, transform.position);
	}
	
	void OnInputAxis(Vector3 input, bool down)
	{
		if (down)
		{
			targetVelocity = input;
			animValue = mTargetVelocity.magnitude;
			moving = true;
		}
		else {
			targetVelocity = new Vector3(0,rigidbody.velocity.y,0);
			moving = false;
		}
		
	}
	
	void OnInput(string input, bool down) 
	{
		if(down)
		{
			switch(input)
			{
			case "RightClick":
				isSprinting = 1;
				break;
			case "Space":
				jumping = true;
				break;
			}
		}
		if(!down)
		{
			switch(input)
			{
			case "RightClick":
				isSprinting = 0;
				break;
			case "Space":
				jumping = false;
				break;
			}
		}
	}
	
	void SetAnimation(string anim, float ammount)
	{
		animObj.SetFloat(anim, ammount);
	}
	
	void SetAnimation(string anim, bool v)
	{
		animObj.SetBool(anim, v);
	}
	
	[RFC(013)]
	void OnSetAnim(float anim)
	{
		SetAnimation("speed", anim);
		//animObj.SetFloat("speed", vel.magnitude);
	}
	
	[RFC(4)]
	void OnSetVel(Vector3 vel)
	{
		mTargetVelocity = vel;
		//SetAnimation("speed", mTargetVelocity.magnitude);
		//animObj.SetFloat("speed", vel.magnitude);
	}
	[RFC(5)]
	void OnSetJump(bool j)
	{
		mJumping = j;
		SetAnimation("isJumping", j);
	}
	[RFC(6)]
	void OnSetSprint(int s)
	{
		mIsSprinting = s;
	}
	[RFC(7)]
	void OnSetTargetImmediate (Vector3 pos)
	{
		transform.position = pos;
	}
}
