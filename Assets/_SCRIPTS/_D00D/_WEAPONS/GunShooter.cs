using UnityEngine;
using TNet;
using System.Collections;

public class GunShooter : TNBehaviour {
	
static public GunShooter instance;

public GameObject[] Guns;
public GameObject cam;
public int mCurrentGun;
public int CurrentGun
{
	set
	{
		tno.Send (28, TNet.Target.All, value);
	}	
}
public bool mShooting;
public int Shooting
{
	set
	{
		tno.Send (29, TNet.Target.All, value);	
	}
}
public int CoolDown = 0;

	//private GUIText info;
	
	void Awake()
	{
		if(TNManager.isThisMyObject)
		{
			instance = this;
		}	
	}
	
	void  Start (){
		if(tno.isMine)
		{
			mCurrentGun = 0;
			//info = GameObject.Find("infotext").guiText;
			//info.text = "Heat Seeker Missile";
		}
	}
	

	void  switchGuns (){
		if(Input.GetKeyDown(KeyCode.Alpha1)){
			instance.CurrentGun = 0;
		
		}
		if(Input.GetKeyDown(KeyCode.Alpha2)){
			instance.CurrentGun = 1;
		
		}
		/*if(Input.GetKeyDown(KeyCode.Alpha3)){
			instance.CurrentGun = 2;
			
		}
		if(Input.GetKeyDown(KeyCode.Alpha4)){
			instance.CurrentGun = 3;
			
		}
		if(Input.GetKeyDown(KeyCode.Alpha5)){
			instance.CurrentGun = 4;
			
		}*/
//RenderGun();
	}


	void  Update (){
		if(tno.isMine)
		{
			switchGuns();
			if(Input.GetMouseButtonUp(0))
				Shooting = 0;
		
			if(Input.GetMouseButtonDown(0))
				Shooting = 1;
		
			if(mShooting){
			
				if (cam)
				{
				Vector3 newRotation = Quaternion.LookRotation(cam.transform.forward - transform.up).eulerAngles;
    			newRotation.x = 0;
    			newRotation.z = 0;
    			transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(newRotation), Time.deltaTime * 300);
    			}
				Guns[mCurrentGun].gameObject.GetComponent<WeaponLauncher>().Shoot(); 
			}
		}
	}
	
	[RFC(28)]
	void OnSetCurrentGun (int i)
	{
		mCurrentGun = i;
	}
	
	[RFC(29)]
	void OnShooting (int i)
	{
		if(i == 1)
		{
			mShooting = true;
		}
		else
		{
			mShooting = false;	
		}
	}
}