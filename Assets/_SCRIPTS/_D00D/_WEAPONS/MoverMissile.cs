using UnityEngine;
using System.Collections;

public class MoverMissile : MonoBehaviour {
		
	public GameObject target;
	public string targettag;
	public float damping= 3.0f;
	public float Speed= 80.0f;
	public float SpeedMax= 80.0f;
	public float SpeedMult = 1.0f;
	public Vector3 Noise = new Vector3(20.0f,20.0f,20.0f);
	private bool  locked;
	public int distanceLock = 70;
	public int DulationLock = 40;
	private int timetorock;
	public bool  Seeker;
	public float LifeTime = 5.0f;
	private float timeCount = 0;
	public float targetlockdirection = 0.5f;
	public bool doNoise;
	
	void  Start (){
		doNoise = false;
		timeCount = Time.time;
		Destroy(gameObject, LifeTime);
		Invoke("DoNoise", 0.3f);
	}
	
	void  Update (){
		if(Time.time>= (timeCount + LifeTime) -0.5f){
			if(this.GetComponent<Damage>()){
				this.GetComponent<Damage>().Active();
			}
		}
		if(Speed < SpeedMax){
			Speed += SpeedMult;
		}
		
		
		Vector3 tmp = new Vector3(transform.forward.x * Speed * Time.deltaTime, transform.forward.y * Speed * Time.deltaTime, transform.forward.z * Speed * Time.deltaTime);
		
		rigidbody.velocity = tmp;
		
		
		if (doNoise)
		{
			ApplyNoise();
		}
		
	    
	}
	
	void DoNoise() 
	{
		doNoise = true;	
	}
	
	void ApplyNoise()
	{
		Vector3 tmp = new Vector3(Random.Range(-Noise.x,Noise.x),Random.Range(-Noise.y,Noise.y),Random.Range(-Noise.z,Noise.z));
		rigidbody.velocity += tmp;
	}
	

}