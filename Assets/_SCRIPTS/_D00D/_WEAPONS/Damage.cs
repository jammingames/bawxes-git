using UnityEngine;
using System.Collections;


public class Damage : MonoBehaviour {
	
	public GameObject effect;
	public GameObject Owner;
	public int dmg = 20;
	public bool Explosive;
	public float ExplosionRadius = 20.0f;
	public float ExplosionForce = 1000.0f;
	private GameObject obj;

	void Start() {
	if(Owner){
		if(Owner.collider){
			Physics.IgnoreCollision(this.collider,Owner.collider);
		}
	}
	}

	public void Active(){
		if(effect){
			//GameObject obj = Instantiate(effect,this.transform.position,this.transform.rotation) as GameObject;
			TNManager.Create(effect, this.transform.position,this.transform.rotation);
			//GameObject.Destr	oy(effect,3);
			//TNManager.Destroy(effect,3);
		}
   	
		if(Explosive)
			ExplosionDamage();
		Destroy(this.gameObject);
	}

	public void ExplosionDamage(){
		Collider[] hitColliders = Physics.OverlapSphere(this.transform.position, ExplosionRadius);
     for (int i = 0; i < hitColliders.Length; i++) {
           Collider hit = hitColliders[i];
	       if (!hit)
	            continue;

	       if(hit.gameObject.tag!="Particle" && hit.gameObject.tag!="Bullet")
		   {
		   	 if(hit.gameObject.GetComponent<DamageManager>()) {
				DamageManager tmpHit = hit.gameObject.GetComponent<DamageManager>();
				tmpHit.ApplyDamage(dmg);
				//hit.gameObject.GetComponent<DamageManager>.ApplyDamage(dmg);
		   	 }
		   }
		   if(hit.rigidbody) {
				Vector3 tmpPos = new Vector3(transform.position.x,transform.position.y,transform.position.z);
		   hit.rigidbody.AddExplosionForce(ExplosionForce, tmpPos, ExplosionRadius, 3.0f);
			}
	 }
	 
}


public void NormalDamage(Collision collision){
		
	if(collision.gameObject.GetComponent<DamageManager>()){
		collision.gameObject.GetComponent<DamageManager>().ApplyDamage(dmg);
	}	
}

void OnCollisionEnter(Collision collision) {
	if(collision.gameObject.tag!="Particle" && collision.gameObject.tag!="Bullet"){
		if(!Explosive)
		NormalDamage(collision);
		Active();
	}
   	
}
}