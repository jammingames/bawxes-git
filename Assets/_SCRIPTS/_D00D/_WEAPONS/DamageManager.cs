using UnityEngine;
using System.Collections;
using TNet;

public class DamageManager : MonoBehaviour
{
	
	public AudioClip[] hitsound;
	public GameObject effect;
	public GameObject effect2;
	public float HP = 100.0f;
	public bool destroy;
	public bool isDead = false;
	public float maxHP;
	private float barLength;
	
	// Use this for initialization
	void Awake() 
	{
		maxHP = HP;
	}
	
	void Start () 
	{
		barLength = Screen.width /2;
	}
	
	
	
	
	
	public void ApplyDamage (float damage) 
	{
		if(hitsound.GetLength(0)>0)
		{
				
		 	AudioSource.PlayClipAtPoint(hitsound[Random.Range(0,hitsound.GetLength(0))], transform.position);
		}
	 	HP -= damage;
	 	if(HP<=0 && !isDead)
		{
	 		Dead();
			isDead=true;
	 	}
	}
	
	public void Dead()
	{
		if(effect)
		TNManager.Create(effect,this.transform.position,this.transform.rotation);
		TNManager.Destroy(this.gameObject);
	}
	
}



