using UnityEngine;
using System.Collections;

public class Blend : MonoBehaviour {
	
	public float duration = 2.0f;
	
	void Start () 
	{
		
	}
	
	void Update () 
	{
		float lerp = Mathf.PingPong(Time.time, duration) / duration;
		this.renderer.material.SetFloat("_Blend", lerp);
	}
}
