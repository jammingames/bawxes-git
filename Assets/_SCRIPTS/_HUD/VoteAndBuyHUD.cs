using UnityEngine;
using System.Collections;

public class VoteAndBuyHUD : MonoBehaviour 
{
	
	public Texture2D voteMenuBase;
	public Texture2D clumpyLocked;
	public Texture2D clumpyNormal;
	public Texture2D clumpySelected;
	public Texture2D clumpyHover;
	public Texture2D clumpyVote;
	public Texture2D aSecondDealLocked;
	public Texture2D aSecondDealNormal;
	public Texture2D aSecondDealSelected;
	public Texture2D aSecondDealHover;
	public Texture2D aSecondDealVote;
	public Texture2D citrusBurnLocked;
	public Texture2D citrusBurnNormal;
	public Texture2D citrusBurnSelected;
	public Texture2D citrusBurnHover;
	public Texture2D citrusBurnVote;
	public Texture2D grenadeLadelLocked;
	public Texture2D grenadeLadelNormal;
	public Texture2D grenadeLadelSelected;
	public Texture2D grenadeLadelHover;
	public Texture2D grenadeLadelVote;
	
	
}
