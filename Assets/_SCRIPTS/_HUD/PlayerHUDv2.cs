using UnityEngine;
using System.Collections;

public class PlayerHUDv2 : TNBehaviour {
	
	public Texture2D HealthBar, StaminaBar, HudSystem, Crosshair, MinimapBorder, Scoreboard;
	
	public GUISkin myHudStyle;
	
//	static public GamePlayer instance;
	
	private DamageManager playerHealth;
	
	private Rect crossPos, minimapPos, healthPos, staminaPos, minimapBorderPos, scoreboardPos, hudSystemPos, warmScorePos, coldScorePos, customMessagePos;
	
	private float healthPercent, staminaPercent, messageTimer;
	private bool firstGo;
	
	private int warmScore, coldScore;
	
	private Camera minimap;
	
	private Vector3 minimapCameraPos;
	
	private Vector2 resolutionScale = new Vector2(1,1);
		
	private Vector2 originalResolution, currentResolution;
	
	private Color crosshairAlpha;
	
	//private GUIStyle myHudStyle;
	
	private bool showingMessage, usingMessageTimer;
	
	public bool _showingMessage
	{
		get{ return showingMessage;}
		set{ showingMessage = value;}
	}
	
	public bool _usingMessageTimer
	{
		get{ return usingMessageTimer;}
		set{ usingMessageTimer = value;}
	}
	
	private string customMessage;
	
	void Start () 
	{
		
		if(tno.isMine)
		{
			
			//myHudStyle = new GUIStyle();
			
			//myHudStyle;
			
			originalResolution = new Vector2(Screen.width,Screen.height);
			currentResolution = originalResolution;
			resolutionScale = new Vector2(currentResolution.x/1920f,currentResolution.y/1080f);
			if(playerHealth)
				rescaleHUD();
			
			minimapPos = new Rect(Screen.width - MinimapBorder.width, 60, 160, 160);
			
			minimapCameraPos = Vector3.zero;
			minimapCameraPos.y += 100;
			
			playerHealth = gameObject.GetComponent<DamageManager>();
			if (GameObject.Find("MinimapCamera"))
				{
				minimap = GameObject.Find("MinimapCamera").camera;
			minimap.transform.localEulerAngles = new Vector3(90, 0, 0);
			minimap.depth = 2;
			minimap.pixelRect = minimapPos;
			((AudioListener) minimap.GetComponent(typeof(AudioListener))).enabled = false;
			minimap.camera.enabled = true;
			}
			rescaleHUD();
			healthPercent = (playerHealth.HP/playerHealth.maxHP)*(HealthBar.width*0.68f);
			staminaPercent = (ControlManager.instance.currStam/ControlManager.instance.maxStam)*(StaminaBar.width*0.68f);
			healthPos = new Rect((HudSystem.width-15)*resolutionScale.x - (HealthBar.width*resolutionScale.x) + (HudSystem.width*0.33f)*resolutionScale.x, Screen.height - (HealthBar.height)*resolutionScale.y+20*resolutionScale.y, healthPercent*resolutionScale.x, (HealthBar.height*0.8f)*resolutionScale.y);
			staminaPos = new Rect((HudSystem.width-15)*resolutionScale.x - (StaminaBar.width*resolutionScale.x) + (HudSystem.width*0.33f)*resolutionScale.x, Screen.height - (StaminaBar.height)*resolutionScale.y+30*resolutionScale.y, staminaPercent*resolutionScale.x, (StaminaBar.height*0.8f)*resolutionScale.y);
		}	
	}
	
	void OnGUI ()
	{
		if(tno.isMine)
		{	
			GUI.skin = myHudStyle;
			GUI.DrawTexture(minimapBorderPos,MinimapBorder);
			GUI.DrawTexture(healthPos, HealthBar);
			GUI.DrawTexture(staminaPos, StaminaBar);
			GUI.DrawTexture(hudSystemPos, HudSystem);
			
			GUI.DrawTexture(scoreboardPos, Scoreboard);
			GUI.Label(warmScorePos,""+warmScore);
			GUI.Label(coldScorePos,""+coldScore);
			
			crosshairAlpha = GUI.color;
			crosshairAlpha.a = 0.6f;
			GUI.color = crosshairAlpha;
			
			GUI.DrawTexture(crossPos,Crosshair);
			
			if(showingMessage)
			{
				GUI.Label(customMessagePos,""+customMessage);
			}
		}
	}
	
	public void SendPlayerHUDMessage(string cString, bool usingTimer)
	{
		customMessage = cString;
		showingMessage = true;
		if(usingTimer)
		{
			messageTimer = 0;
			usingMessageTimer = true;
		}
		
	}
	
	private void hidePlayerMessage()
	{
		messageTimer = 0;
		usingMessageTimer = true;
		
	}

		
	private void rescaleHUD()
	{
		//	myHudStyle.fontSize = Mathf.CeilToInt(36f*resolutionScale.x);
			myHudStyle.label.fontSize = Mathf.CeilToInt(36f*resolutionScale.x);
		
			crossPos = new Rect(Screen.width/2 - (Crosshair.width/2)*resolutionScale.x, Screen.height/2 - (Crosshair.height/2)*resolutionScale.y, Crosshair.width*resolutionScale.x, Crosshair.height*resolutionScale.y);
			
			minimapPos = new Rect(Screen.width - MinimapBorder.width*(resolutionScale.x) + 40*resolutionScale.x, 42*(resolutionScale.y), 180*(resolutionScale.x), 180*(resolutionScale.y));
			minimap.pixelRect = minimapPos;
			minimapBorderPos = new Rect(Screen.width - MinimapBorder.width*(resolutionScale.x), Screen.height - MinimapBorder.height*(resolutionScale.y), MinimapBorder.width*(resolutionScale.x), MinimapBorder.height*(resolutionScale.y));
		
			hudSystemPos = new Rect(15, Screen.height - HudSystem.height*resolutionScale.y, HudSystem.width*resolutionScale.x, HudSystem.height*resolutionScale.y);
			
			scoreboardPos = new Rect(Screen.width/2 - (Scoreboard.width/2*resolutionScale.x),0, Scoreboard.width*resolutionScale.x,Scoreboard.height*resolutionScale.y);
			warmScorePos = new Rect(Screen.width/2 - Scoreboard.width/4*resolutionScale.x, Scoreboard.height/4*resolutionScale.y, Scoreboard.width/2*resolutionScale.x, Scoreboard.height*resolutionScale.y);
			coldScorePos = new Rect(Screen.width/2 + Scoreboard.width/4*resolutionScale.x, Scoreboard.height/4*resolutionScale.y, Scoreboard.width/2*resolutionScale.x, Scoreboard.height*resolutionScale.y);
			
			if(showingMessage)
				customMessagePos = new Rect(Screen.width/2 - (customMessage.Length/2*(20f*resolutionScale.x)), Scoreboard.height*resolutionScale.y*1.5f, customMessage.Length, Scoreboard.height*resolutionScale.y*1.5f);
			
			minimapCameraPos = ControlManager.instance.transform.position;
			minimapCameraPos.y = 75;
			minimap.transform.position = minimapCameraPos;
			
			Vector3 tmpRot = new Vector3(minimap.transform.eulerAngles.x,ControlManager.instance.transform.eulerAngles.y,minimap.transform.eulerAngles.z);
			minimap.transform.eulerAngles = tmpRot;
	}
	
	void Update () 
	{
		if(tno.isMine)
		{
			currentResolution = new Vector2(Screen.width,Screen.height);
			resolutionScale = new Vector2(currentResolution.x/1920f,currentResolution.y/1080f);
		//	print (""+Screen.width+","+""+Screen.height);
			
		/*	if(Application.isPlaying)
			{
				Screen.lockCursor = true;
				Screen.showCursor = false;
			}
			
			else
			{
				Screen.lockCursor = false;
				Screen.showCursor = true;
			}
			*/
			
			if(currentResolution != originalResolution)
			{
				rescaleHUD();
				originalResolution = currentResolution;
			}
			
			healthPercent = (playerHealth.HP/playerHealth.maxHP)*(HealthBar.width*0.68f);
			staminaPercent = (ControlManager.instance.currStam/ControlManager.instance.maxStam)*(StaminaBar.width*0.68f);
			healthPos = new Rect((HudSystem.width-15)*resolutionScale.x - (HealthBar.width*resolutionScale.x) + (HudSystem.width*0.33f)*resolutionScale.x, Screen.height - (HealthBar.height)*resolutionScale.y+20*resolutionScale.y, healthPercent*resolutionScale.x, (HealthBar.height*0.8f)*resolutionScale.y);
			staminaPos = new Rect((HudSystem.width-15)*resolutionScale.x - (StaminaBar.width*resolutionScale.x) + (HudSystem.width*0.33f)*resolutionScale.x, Screen.height - (StaminaBar.height)*resolutionScale.y+30*resolutionScale.y, staminaPercent*resolutionScale.x, (StaminaBar.height*0.8f)*resolutionScale.y);
			
			minimapCameraPos = ControlManager.instance.transform.position;
			minimapCameraPos.y = 75;
			minimap.transform.position = minimapCameraPos;
			
			Vector3 tmpRot = new Vector3(minimap.transform.eulerAngles.x,ControlManager.instance.transform.eulerAngles.y,minimap.transform.eulerAngles.z);
			minimap.transform.eulerAngles = tmpRot;
			
			if(usingMessageTimer)
			{
				messageTimer += Time.deltaTime;
				if(messageTimer >= 3)
				{
					messageTimer = 0;
					showingMessage = false;
					usingMessageTimer = false;
				}
			}
		}
	}
}
