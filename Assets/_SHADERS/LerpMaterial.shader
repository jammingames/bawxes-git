    Shader "Toon/Lerp" {
     
    Properties {
    _Color ("Main Color", Color) = (.5,.5,.5,1)
    _OutlineColor ("Outline Color", Color) = (0,0,0,1)
	_Outline ("Outline width", Range (.002, 0.03)) = .005
    _MainTex ("First Texture", 2D) = "white" {}
    _BlendTex ("Second Texture", 2D) = "white" {}
    _Blend ("Blend", Range(0,1)) = 0.0
     
    }
	
    SubShader {
    Tags { "RenderType"="Opaque" }
    	UsePass "Toon/Lighted/FORWARD"
		UsePass "Toon/Basic Outline/OUTLINE"
    LOD 200
    CGPROGRAM
    #pragma surface surf Lambert
     
    sampler2D _MainTex;
    sampler2D _BlendTex;
    float _Blend;
     
    struct Input {
    float2 uv_MainTex;
    float2 uv_BlendTex;
    };
     
     
    void surf(Input IN, inout SurfaceOutput o) {
     
    half4 c1 = tex2D(_MainTex, IN.uv_MainTex);
    half4 c2 = tex2D(_BlendTex, IN.uv_BlendTex);
    o.Albedo = (c1.rgb * (1-_Blend)) + (c2.rgb * _Blend);
    o.Alpha = 1;
     
    }
     
    ENDCG
    
    }
    
    Fallback "Toon/Basic"
     
    }